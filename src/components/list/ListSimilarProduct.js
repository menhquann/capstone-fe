import { useState, useEffect } from 'react';
import {
    listActiveProducts,
    listIdsSimilar,
    listProductsByIds,
    listSimilarProductsById,
} from '../../apis/product';
import Loading from '../ui/Loading';
import Error from '../ui/Error';
import ProductCard from '../card/ProductCard';

const ListSimilarProducts = ({
    heading = 'Best Seller',
    col = 'col-xl-2-5 col-md-3 col-sm-4 col-6',
    productId = '64917e6f3b60d79d805d5e54',
    limit = '5',
}) => {
    const [isloading, setIsLoading] = useState(false);
    const [error, setError] = useState('');

    const [products, setProducts] = useState([]);

    const init = () => {
        setError('');
        setIsLoading(true);
        // listIdsSimilar(1)
        //     .then((data) => {
        //         console.log(data, 'data');
        //         if (data.error) {
        //             console.log(data.error, 'data.error');
        //             setError(data.error);
        //         } else {
        //             console.log('do list id', 'data');
        //             return listProductsByIds([
        //                 '647174dbdaad69735a8cdd0c',
        //                 '6470e47adaad69735a8cd94b',
        //             ]);
        //         }
        //     })
        listSimilarProductsById(productId)
            .then((data) => {
                console.log('listProductsByIds', data);
                if (data.error) setError(data.error);
                else setProducts(data);
                setIsLoading(false);
            })
            .catch((error) => {
                setError('Server Error');
                setIsLoading(false);
            });
    };

    useEffect(() => {
        init();
    }, [productId]);

    return (
        <div className="position-relative">
            {heading && <h4>{heading}</h4>}

            {isloading && <Loading />}
            {error && <Error msg={error} />}

            <div className="row mt-3">
                {products &&
                    products.map((product, index) => (
                        <div className={`${col} mb-4`} key={index}>
                            <ProductCard product={product} />
                        </div>
                    ))}
            </div>
        </div>
    );
};

export default ListSimilarProducts;
